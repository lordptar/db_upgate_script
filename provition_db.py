#!/usr/bin/python3

import sqlite3

with sqlite3.connect('example.db') as conn:
    c = conn.cursor()

    # Create version_db table
    c.execute('''CREATE TABLE version_db
                 (id integer, current_version integer)''')

    # Create version_db table names
    c.execute('''CREATE TABLE names
                 (ids integer, names text)''')

    # Insert a row of data
    c.execute("INSERT INTO version_db VALUES (0, 0)")

    # Save (commit) the changes
    conn.commit()
