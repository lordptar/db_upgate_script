#!/usr/bin/python3

import glob
import sqlite3
from sqlite3 import Error
import sys

path_to_db = 'example.db'
db_version = None
sql_files_path = "sql_scripts/*.sql"


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def get_db_curent_version(conn):
    cur = conn.cursor()
    cur.execute("SELECT current_version FROM version_db WHERE id=0")

    version_db = int(cur.fetchall()[0][0])
    return version_db


def get_script_number(file_name):
    number = int(file_name.split("/")[-1].split(".", 1)[0])
    return number


def get_sql_files(sql_files_path, db_version):
    sql_files = sorted(glob.glob(sql_files_path))
    n = get_script_number(sql_files[-1])
    if n <= db_version:
        print("Nothing to do. The DB has already had the newest upadate")
        sys.exit(0)
    return sql_files


def update_db_version(conn, db_version):
    sql = ''' UPDATE version_db
              SET current_version=?
              WHERE id = 0'''
    cur = conn.cursor()
    cur.execute(sql, str(db_version))


def run_scripts(conn, sql_files, db_version):
    for sql_file in sql_files:
        n = get_script_number(sql_file)
        try:
            if n > db_version:
                with open(sql_file) as db:
                    sql_statement = db.read()
                    conn.executescript(sql_statement)
                conn.commit()
                db_version = n
                update_db_version(conn, db_version)
                print("%s ran successfully" % sql_file)
        except TypeError as e:
            print(e)


def main():
    conn = create_connection(path_to_db)
    with conn:
        print("1. Get version of DB:")
        db_version = get_db_curent_version(conn)

        sql_files = get_sql_files(sql_files_path, db_version)

        print("2. Run Scripts")
        run_scripts(conn, sql_files, db_version)


if __name__ == '__main__':
    main()
