# Upgrade database by consequential scripts

## Use Case:
 
- A database upgrade requires the execution of numbered scripts stored in a folder. E.g. 045.createtable.sql
 
- There may be holes in the numbering and there isn't always a . after the number.
 
- The database upgrade works by looking up the current version in the database. It then compares this number to the scripts.
 
- If the version number from the db matches the highest number from the script then nothing is executed.
 
- If the number from the db is lower than the highest number from the scripts, then all scripts that contain a higher number than the db will be executed against the database.
 
- In addition the database version table is updated after the install with the latest number.

## Solution
   As a solution I created the upgrade_db.py script.
   The upgrade_db.py script picks up script from the sql_scripts folder and updates the example.db database

   To test the upgrade_db.py script:

   - run provition_db.py. Which will create the example.db database and 2 tables version_db and names. The names table is for mock insert SQL scripts.

   - run upgrade_db.py. The upgrade_db.py script will run mock insert SQL scripts consequentially from the sql_scripts folder

   - if you run the upgrade_db.py script again, the script will not run any sql script but if you decrease the version number in the version_db table and run the upgrade_db.py script again, the script will run SQL scripts with a higher number than the current version of the database.